# Dockerized aports-turbo

## Depends

Any recent version of docker and docker compose.
This compose file is designed with Traefik in mind but any other load balancer (ie nginx) would do.

## How to run

To run aports-turbo with 4 instances run the following.

``` docker-compose up -d --scale http=4 ```

This should start 4 http services and 1 update service.

