FROM alpine:3.11

ENV VERSION=master

COPY patch/disable-hostname-validation.patch /tmp

RUN apk add --no-cache \
	su-exec \
	lua-turbo \
	lua-sqlite \
	lua-lustache \
	lua-socket \
	lua-gversion && \
	mkdir -p /var/www/localhost/htdocs /etc/ssl/certs && \
	ln -sf /etc/ssl/cert.pem /etc/ssl/certs/ca-certificates.crt && \
	wget -qO- https://gitlab.alpinelinux.org/alpine/infra/aports-turbo/-/archive/$VERSION/aports-turbo-$VERSION.tar.gz | \
	tar -zx --strip-components=1 -C /var/www/localhost/htdocs && \
	patch -p0 -i /tmp/disable-hostname-validation.patch && \
	chmod +x /var/www/localhost/htdocs/tools/*.lua

COPY cron /etc/periodic

WORKDIR /var/www/localhost/htdocs

EXPOSE 8080
